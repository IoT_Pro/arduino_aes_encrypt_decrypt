#include <AESLib.h>

uint8_t key[] = "aaaabbbbccccdddd";
uint8_t iv[] = "1234567812345678";
char text[] = "thankyasou";
char new_text[17];

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

  // reconfigure text
  reconfigure_plaintext();

  // aes encoding
  aes128_cbc_enc(key, iv, new_text, 16);  
}

void loop() {
  Serial.println(new_text);
  delay(1000);
}

void reconfigure_plaintext() {
  String str(text);
  for (int i=0; i < 17 - sizeof(text); i++) {
    str += " ";
  }
  str.toCharArray(new_text, 17);
}
